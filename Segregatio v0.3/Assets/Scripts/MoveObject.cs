﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour 
{
    //private Transform ThisTransform;
    public IObject obj;
    public float Speed;

    public Vector3 Destination;

    void Start()
    {
        Destination = this.transform.position;
    }

    void Update()
    {
        if (Destination != this.transform.position)
        {
            MoveUpdate(); 
        }
    }

    public void MoveUpdate()
    {
        if (obj.Active)
        {
            transform.position = Vector3.MoveTowards(this.transform.position,
                                                     Destination,
                                                     Speed * Time.deltaTime);
        }
    }
}
