﻿using UnityEngine;
using System.Collections;

public class IObject : MonoBehaviour 
{
    public bool ObjectProcessed = false;
    public bool Active = true;
    public bool Segregated = false;
    public bool Allocated = false;

    public WasteType.Waste TrashType;

    void Start()
    {
        GetComponent<Renderer>().enabled = false;
    }
}
