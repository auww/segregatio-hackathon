﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TrashStatPrefab
{
    public Sprite TrashImage;
    public int SpriteID;
    public WasteType.Waste TrashType;
}

public class ObjectRenderController : MonoBehaviour
{
    public ChuteObjectsSpawner ChuteObjSpawner;
    public List<TrashStatPrefab> TrashSprites;

    public int ObjectVisibilityLimit;

    void Start()
    {
        Invoke("AllocateObjectSprite", 0.1f);
        Invoke("ObjectVisibilityChecker", 0.15f);
    }

    void Update()
    {
        ObjectVisibilityChecker();
    }

    void AllocateObjectSprite()
    {
        foreach (GameObject go in ChuteObjSpawner.TrashObjects)
        {
            TrashStatPrefab sprite = GetSprite(Random.Range(0, TrashSprites.Count));
            go.GetComponent<SpriteRenderer>().sprite = sprite.TrashImage;
            go.GetComponent<IObject>().TrashType = sprite.TrashType;
            go.name = sprite.SpriteID.ToString();
        }
    }

    public void ObjectVisibilityChecker()
    {
        for (int i = 0; i < ChuteObjSpawner.SpawnCount; i++)
        {
            Renderer trashRenderer = ChuteObjSpawner.TrashObjects[i].GetComponent<Renderer>();

            if (i > ObjectVisibilityLimit)// && !chuteObjSpawner.TrashObjects[i].GetComponent<Object>().Segregated)
            {
                trashRenderer.enabled = false;
            }

            else
            {
                trashRenderer.enabled = true;
            }

            if (ChuteObjSpawner.TrashObjects[i].GetComponent<IObject>().Segregated || !ChuteObjSpawner.TrashObjects[i].GetComponent<IObject>().Active)
            {
                trashRenderer.enabled = false;
            }

            /*if (SpawnedObjectScript[i].transform.position == SpawnedObjectScript[i].GetDestinationPosition ().position &&
                SpawnedObjectScript[i].ProcessObject == true)
            {
                SpawnedObjectScript[i].renderer.enabled = false;
            }*/
        }
    }

    TrashStatPrefab GetSprite(int id)
    {
        return TrashSprites.Where(p => p.SpriteID == id).Select(p => p).SingleOrDefault();
    }
}
