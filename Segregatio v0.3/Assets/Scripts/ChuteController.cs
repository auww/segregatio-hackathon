﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChuteController : MonoBehaviour
{
    public ChuteObjectsSpawner chuteObjSpawner;
    public float PosNodeDistance;

    void Start()
    {
        chuteObjSpawner.SpawnObjects();
        InitialObjectPlacements();
    }
    void Update()
    {
        if (chuteObjSpawner.TrashObjects[0].GetComponent<MoveObject>().Destination == chuteObjSpawner.TrashObjects[0].transform.position && 
            chuteObjSpawner.TrashObjects[0].GetComponent<IObject>().Allocated)
        {
            chuteObjSpawner.TrashObjects[0].GetComponent<IObject>().Segregated = true;
        }
        
        UpdateObjectPlacement();
    }

    void InitialObjectPlacements()
    {
        for (int i = 1; i < chuteObjSpawner.SpawnCount; i++)
        {
            chuteObjSpawner.PositionNodes[i].transform.position = new Vector3(chuteObjSpawner.PositionNodes[i - 1].transform.position.x,
                                                                              chuteObjSpawner.PositionNodes[i - 1].transform.position.y,
                                                                              chuteObjSpawner.PositionNodes[i - 1].transform.position.z + PosNodeDistance);

            chuteObjSpawner.TrashObjects[i].transform.position = chuteObjSpawner.PositionNodes[i].transform.position;
        }
    }

    public void UpdateTargetPosition(GameObject target)
    {
        //chuteObjSpawner.TrashObjects[0].GetComponent<Object>().Segregated = true;
        chuteObjSpawner.TrashObjects[0].GetComponent<MoveObject>().Destination = target.transform.position;
        UpdateObjectPosition();
    }

    void UpdateObjectPlacement()
    {
        for (int i = 0; i < chuteObjSpawner.SpawnCount; i++)
        {
            if (chuteObjSpawner.TrashObjects[i].GetComponent<IObject>().Segregated &&
                !chuteObjSpawner.TrashObjects[i].GetComponent<IObject>().ObjectProcessed)
            {
                GameObject TempGO = chuteObjSpawner.TrashObjects[i];

                for (int j = i; j < chuteObjSpawner.SpawnCount - 1; j++)
                {
                    chuteObjSpawner.TrashObjects[j] = chuteObjSpawner.TrashObjects[j + 1];
                }

                chuteObjSpawner.TrashObjects[chuteObjSpawner.SpawnCount - 1] = TempGO;
                chuteObjSpawner.TrashObjects[chuteObjSpawner.SpawnCount - 1].GetComponent<IObject>().ObjectProcessed = true;
            }
        }
    }

    void UpdateObjectPosition()
    {
        for (int i = 1; i < chuteObjSpawner.SpawnCount; i++)
        {
            IObject obj = chuteObjSpawner.TrashObjects[i].GetComponent<IObject>();

            if (!obj.Segregated && obj.Active)
            {
                chuteObjSpawner.TrashObjects[i].GetComponent<MoveObject>().Destination = chuteObjSpawner.PositionNodes[i - 1].transform.position;
            }
        }
    }
}
