﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DescriptionUI : MonoBehaviour
{

    public string Description;
    public GameObject DescriptionUIObject;
  

    public void SendDescription()
    {
        DescriptionUIObject.GetComponent<TestClick>().CheckSkill(gameObject.GetComponent<DescriptionUI>());

    }
}
