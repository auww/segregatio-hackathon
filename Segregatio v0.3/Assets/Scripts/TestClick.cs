﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class TestClick : MonoBehaviour 
{
  
    public List<DescriptionUI> Skills;

    private GameObject TextBoxGO;
    private Text TextBox;

    private Text ReqLevelText;
    private GameObject ReqLevelGO;

    private Text RemPointText;
    private GameObject RemPointGO;


    // Use this for initialization
    void Start()    
    {
        Skills = new List<DescriptionUI>();
        TextBoxGO = GameObject.Find("Description Text");
        TextBox = TextBoxGO.GetComponent<Text>();

     
    }

    // Update is called once per frame
   

    public void CheckSkill(DescriptionUI target)
    {
        TextBox.text = target.Description;
    }

   
}