﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WasteType
{
    public enum Waste
    {
        Biodegradable,
        NonBiodegradable
    }
}
