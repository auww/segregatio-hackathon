﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LoadingScreen : MonoBehaviour {

    public string levelToLoad;
    public GameObject background;
    public GameObject text;
    public GameObject progressBar;

    private int loadProgress = 0;
	// Use this for initialization
	void Start () {
        background.SetActive(false);
        text.SetActive(false);
        progressBar.SetActive(false);
	}
	
	// Update is called once per frame
	public void SkillClicked (Button button) {
        if (button.tag == "Candy Wrapper")
        { 
            Debug.Log("hi");
            StartCoroutine(DisplayLoadingScreen(levelToLoad));
           
        //Application.LoadLevel(levelToLoad);
        }
	}

    IEnumerator DisplayLoadingScreen(string level)
    {
        background.SetActive(true);
        text.SetActive(true);
        progressBar.SetActive(true);

        progressBar.transform.localScale = new Vector3(loadProgress, progressBar.transform.position.y, progressBar.transform.position.z);


        text.GetComponent<GUIText>().text = "Loading Progress: " + loadProgress + "%";
        
        AsyncOperation async = Application.LoadLevelAsync(level);
       
        while (!async.isDone)
        {
            loadProgress = (int) (async.progress * 100);
            text.GetComponent<GUIText>().text = "Loading Progress: " + loadProgress + "%";
            progressBar.transform.localScale = new Vector3(async.progress, progressBar.transform.position.y, progressBar.transform.position.z);
            yield return null;
        }
    }

}
