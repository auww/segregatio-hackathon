﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChuteObjectsSpawner : MonoBehaviour
{
    public GameObject Container;
    public GameObject PositionContainer;

    public int SpawnCount;

    public GameObject TrashPrefab;
    public GameObject NodePrefab;

    public List<GameObject> TrashObjects;
    public List<GameObject> PositionNodes;

    public void SpawnObjects()
    {
        for (int i = 0; i < SpawnCount; i++)
        {
            TrashObjects.Add(Spawner.Spawn(TrashPrefab, this.transform.position));
            PositionNodes.Add(Spawner.Spawn(NodePrefab, this.transform.position));
        }

        PlaceIntoContainers();
    }

    void PlaceIntoContainers()
    {
        foreach (GameObject go in TrashObjects)
        {
            go.transform.parent = Container.transform;
        }

        foreach (GameObject go in PositionNodes)
        {
            go.transform.parent = PositionContainer.transform;
        }
    }


}
