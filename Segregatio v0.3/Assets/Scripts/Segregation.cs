﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Segregation : MonoBehaviour 
{
    public ChuteController chute;
    public ChuteObjectsSpawner chuteObjSpawner;
	public Text countText;

	private int count = 0;

	void start()
	{

		SetCountText ();
	}

	public void AllocateTrash(GameObject target)
    {
        if (CheckAllocation(target))
        {
            chute.UpdateTargetPosition(target);
            chuteObjSpawner.TrashObjects[0].GetComponent<IObject>().Allocated = true;
			SetCountText();

		}
    }					

    bool CheckAllocation(GameObject target)
    {
        if (chuteObjSpawner.TrashObjects[0].GetComponent<IObject>().TrashType == target.GetComponent<Bin>().TrashType)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

	void SetCountText()
	{
		count += 1;
		countText.text = "Score: " + count.ToString ();
		//D
	}

}
