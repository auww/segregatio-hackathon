﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameChecker : MonoBehaviour 
{
	public Text TimerText;
	public float Timer = 30.0f;
	public bool Counting = true;

	public ChuteObjectsSpawner ChuteObjSpawner;

	void Update () 
	{
		if (Counting)
		{
			Timer -= 1.0f * Time.deltaTime;

			TimerTextDisplay();
		}

		if (Timer <= 0.0f)
		{
			Counting = false;
		}

		CheckGame();
	}

	void CheckGame()
	{
		if (CheckAllSegregated())
		{
			Application.LoadLevel(Application.loadedLevel + 1);
		}

		else
		{
			if (!Counting)
			{
				Application.LoadLevel(0);
			}
		}
	}

	void TimerTextDisplay()
	{

		TimerText.text = "Timer: " + Timer.ToString ();
	}

	public bool CheckAllSegregated()
	{
		int i = 0;
		foreach (GameObject go in ChuteObjSpawner.TrashObjects)
		{
			if(!go.GetComponent<IObject>().Segregated)
			{
				i++;
			} 	
		}
		
		if (i > 0)
		{
			return false;
		}
		
		else
		{
			return true;
		}
	}
}
