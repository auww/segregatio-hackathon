﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
    public static GameObject Spawn(GameObject obj, Vector3 pos)
    {
        return (GameObject)Instantiate(obj,
                                       pos,
                                       Quaternion.identity);
    }
}
